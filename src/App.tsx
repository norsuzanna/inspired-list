import React, { useState, FunctionComponent } from "react";
import "./App.css";
import { List, Button, Form } from "antd";
import { Formik } from "formik";
import * as Yup from "yup";
import TextInput from "./TextInput";

const formik = {
  initialValues: {
    name: ""
  },
  validationSchema: Yup.object().shape({
    name: Yup.string().required()
  })
};

let randomName = "";

const App: FunctionComponent<{}> = () => {
  const [names, setNames] = useState([
    "Ahmad",
    "Lee",
    "Muthu",
    "Peter",
    "Maher"
  ]);
  const [isWinnerSelected, handleChangeWinner] = useState(false);

  // Remove existing name
  const removeExistingName = (name: string) => {
    setNames(names.filter(item => item !== name));
  };

  // Add new name
  const addNewName = (values: any, { resetForm }: { resetForm: any }) => {
    setNames([...names, values.name]);
    resetForm(formik.initialValues);
  };

  // Select a winner
  const selectAWinner = () => {
    handleChangeWinner(isWinnerSelected => !isWinnerSelected);

    if (!isWinnerSelected) {
      let currentName = randomName;
      while (currentName === randomName) {
        currentName = names[Math.floor(Math.random() * names.length)];
      }
      randomName = currentName;
    }
  };

  return (
    <div>
      <header className="App-header">
        <Button
          type={isWinnerSelected ? "danger" : "primary"}
          size="large"
          onClick={selectAWinner}
        >
          {isWinnerSelected ? "Back to the list" : "Choose a winner"}
        </Button>
        {isWinnerSelected ? (
          <div className="Winner">
            <h2>The winner is {randomName}</h2>
          </div>
        ) : (
          <List
            style={{ margin: "20px 0" }}
            header={
              <div className="Header">
                <h2>List of nominees</h2>
              </div>
            }
            footer={
              <Formik {...formik} onSubmit={addNewName}>
                {form => (
                  <Form
                    onSubmit={form.handleSubmit}
                    layout="inline"
                    className="TextInput-Container"
                  >
                    <TextInput
                      {...form}
                      name="name"
                      placeholder="Enter a name"
                      type="text"
                    />
                    <Button type="primary" htmlType="submit">
                      Add
                    </Button>
                  </Form>
                )}
              </Formik>
            }
            bordered
            dataSource={names}
            renderItem={name => (
              <List.Item className="List-Item">
                <div className="Footer-Container">
                  <p className="Item-Name">{name}</p>
                  <Button
                    type="danger"
                    onClick={() => removeExistingName(name)}
                  >
                    Delete
                  </Button>
                </div>
              </List.Item>
            )}
          />
        )}
      </header>
    </div>
  );
};

export default App;
