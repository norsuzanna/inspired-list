import React, { FunctionComponent } from "react";
import { Form, Input } from "antd";

interface Props {
  values?: any;
  errors?: any;
  setFieldValue?: any;
  setFieldTouched?: any;
  name?: any;
  placeholder?: any;
  type?: any;
  prefix?: any;
  label?: any;
  max?: any;
  autoFocus?: any;
  onChange?: any;
  addonAfter?: any;
  addonBefore?: any;
  size?: any;
  disabled?: any;
  className?: any;
  touched?: any;
  style?: any;
  defaultValue?: any;
  step?: any;
  min?: any;
}

const TextInput: FunctionComponent<Props> = props => {
  return (
    <Form.Item
      validateStatus={
        props.disabled
          ? "success"
          : props.touched[props.name] && props.errors[props.name] && "error"
      }
      label={props.label}
      className={props.className}
      style={props.style}
    >
      <Input
        disabled={props.disabled}
        prefix={props.prefix}
        addonAfter={props.addonAfter}
        addonBefore={props.addonBefore}
        placeholder={props.placeholder}
        value={props.values[props.name]}
        min={props.min}
        step={props.step}
        autoFocus={props.autoFocus}
        defaultValue={props.defaultValue}
        onChange={
          props.onChange
            ? props.onChange
            : event => {
                if (event.target.value.length === props.max + 1) {
                  return;
                }
                props.setFieldValue(props.name, event.target.value);
                props.setFieldTouched(props.name, true, false);
              }
        }
        type={props.type}
        size={props.size}
      />
    </Form.Item>
  );
};

export default TextInput;
